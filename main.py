from flask import Flask
from flask import render_template
from flask import request
#from flask import jsonfy
import requests
import json

app = Flask(__name__)

app.run(debug=True)

@app.route('/')
def landing():
    return render_template('index.html')


@app.route('/', methods = ['POST', 'GET'])
def homepage ():
    city = request.form['city']

    if city == "":
        return render_template('index.html')

    response = requests.get("http://api.openweathermap.org/data/2.5/weather?q="+city+"&appid=cb72a8c02207f98cfda2547150dcabec&units=metric")
    json_data = json.loads(response.text)
    temperature = json_data['main']['temp']
    feel_temperature = json_data['main']['feels_like']
    #conditions = json_data['weather']['description']

    output = [temperature, feel_temperature]
    return render_template('details.html', output=output)

@app.route('/details')
def details ():
        return render_template('details.html')